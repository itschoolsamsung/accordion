package com.example.accordion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<HashMap<String, Object>> data = new ArrayList<>(3);
        HashMap<String, Object> map;

        map = new HashMap<String, Object>();
        map.put("id", 1);
        map.put("name", "Ivanoff");
        map.put("checked", true);
        data.add(map);

        map = new HashMap<String, Object>();
        map.put("id", 2);
        map.put("name", "Petroff");
        map.put("checked", false);
        data.add(map);

        map = new HashMap<String, Object>();
        map.put("id", 3);
        map.put("name", "Petrosian");
        map.put("checked", true);
        data.add(map);

        String[] from = { "id", "name", "checked" };
        int[] to = { R.id.textId, R.id.textName, R.id.checkBox };

        SimpleAdapter adapter = new SimpleAdapter(this, data, R.layout.item, from, to);
        ListView lv = (ListView)findViewById(R.id.listView);
        lv.setAdapter(adapter);
    }
}
